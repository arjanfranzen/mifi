import base64

from Crypto.Cipher import PKCS1_v1_5, DES3
from Crypto.PublicKey import RSA

key = RSA.importKey("""-----BEGIN PUBLIC KEY-----
MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgQC+uQ+K9dinx9qMp0rEPh7opI5o
YMDUal1pC+oILjp04VcfLFjpTuM5hipJqBGjG7Skj0GzvN/QVMNEO7YQtUGLPLr6
55NuG+Kv0uDfhlpuWcK43x6NVwJWfQqWUMsHpD3jkCCWnfCZf8pYfZqK5GJ88YR3
7AZ2XfOqj7RZ3Uya8wICJxE=
-----END PUBLIC KEY-----""")

cipher = PKCS1_v1_5.new(key)
print(base64.b64encode(cipher.encrypt(b'mysecretpassword!')))

#publickey = key.publickey # pub key export for exchange

#encrypted = key.encrypt("encrypt this message", 1)
#message to encrypt is in the above line 'encrypt this message'

#print ('encrypted message:', encrypted)  #ciphertext)

f = open ('encryption.txt', 'w')
f.write(str(encrypted)) #write ciphertext to file
f.close()

