#! /bin/bash
 
#
# Send SMSes using a Huawei gateway, via cURL.
#
# kevin.lamontagne@libeo.com 2013-05-15
 
set -e
set -u
 
function usage(){
 /bin/cat <<EOF
Usage: $0 -t number [-H hostname] [-p password] [-m "message"] [-v]
Arguments:
-h : show this help
-H : hostname (or IP) for the SMS gateway
-p : password for admin
-t : destination (SMS number) (required)
-m : message (will be converted to 7-bit)
-v : verbose; show the curl output
EOF
 exit -1;
}
 
verbose=""
myhost="192.168.1.1"
# encoded using RSA crypt.py to no avail
password="sXbUXWGfyKQagavr0/nyYio+Wx1QlsJH1WuIqeLTCmjDU/YNZWeBomz6RzudSyRmURtu+SKggoyM0evuYnc8Hnsiurl1ljHgecBOaamj8+Ec+PNhrLSDEi7NMzzLKThBTVZdVGEss3grwbpVDwT+pCaoKaVFrbJMQc1u4YM062M="
message="Test SMS"
mlen="${#message}"
destination=""
 
# Parse args
OPTERR=0
#only "-t" requires a value
while getopts ":hH:p:t::m:v" opt
do
case $opt
 in
 t)
 destination="$OPTARG"
 ;;
 m)
 message="$OPTARG"
 ;;
 H)
 myhost="$OPTARG"
 ;;
 v)
 verbose="--verbose"
 ;;
 d)
 dry=yes
 verbose=yes
 ;;
 h)
 usage
 ;;
 \?)
 echo "Invalid option: $OPTARG"
 usage
 ;;
 echo "-${OPTARG}: Argument required"
 usage
 ;;
 esac
done
if [ "$OPTERR" != "0" -o -z "$destination" ]
then
 usage
fi
 
# http://stackoverflow.com/questions/296536
rawurlencode() {
 local string="${1}"
 local strlen=${#string}
 local encoded=""
 
 for (( pos=0 ; pos<strlen ; pos++ )); do
 c=${string:$pos:1}
 case "$c" in
 [-_.~a-zA-Z0-9] ) o="${c}" ;;
 * ) printf -v o '%%%02x' "'$c"
 esac
 encoded+="${o}"
 done
 echo "${encoded}"
}
 
#Convertir en ASCII 7-bit
message="$(echo "$message" | /usr/bin/iconv -c --to-code="ASCII")"
#Couper a 154 chars
message="$(echo "$message" | cut -c1-154)"
#Encoder pour le GET
message="$(rawurlencode "$message")"
 
#Create a temporary cookiejar
cookiejar="$(mktemp)"
cat /etc/icinga/huawei_default_cookie.txt > "$cookiejar"
 
#Login with a default cookiejar
curl -s $verbose --cookie-jar "$cookiejar" --cookie "$cookiejar" --data "Username=admin&Password=${password}" "http://${myhost}/index/login.cgi" >/dev/null
 
#Need to retrieve this page before sending a SMS...
curl -s $verbose --cookie-jar "$cookiejar" --cookie "$cookiejar" "http://${myhost}/html/sms/message.asp" >/dev/null
 
#HTTP POST the SMS
url_send="http://${myhost}/html/sms/sendSms.cgi?RequestFile=/html/sms/message.asp"
data_send="Receivers=${destination}&SmsContent=${message}&EncodeType=7bit&LengthList=${mlen},0,0,0&CurrentTime=$(date '+%D %r' | sed 's/ $//')"
command_send='LC_TIME="en_US.UTF-8" curl -s --data "$data_send" "$url_send" --cookie "$cookiejar"'
if ! eval $command_send | grep "gVarSendStateOK" -c
then
 echo "Erreur dans l'envoi de sms"
 exit 2
fi
 
rm -f "$cookiejar"
